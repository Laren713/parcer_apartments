import requests
from peewee import InternalError
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from bs4 import BeautifulSoup
from time import sleep

from db_model import Flats, db

headers = {'User-Agent': 'CroockedHands/2.0 (EVM x8), CurlyFingers20/1;p'}


url = "https://www.kijiji.ca/b-apartments-condos/city-of-toronto/c37l1700273"

browser = Chrome(executable_path='#path to chromedriver')
browser.get(url)

response = requests.get(url, headers=headers)
soup = BeautifulSoup(response.text, 'lxml')

count_of_pages = soup.find('span', class_='resultsShowingCount-1707762110').text.split(' ')[5]

try:
    db.connect()
    Flats.create_table()
except InternalError as IE:
    print(str(IE))

for page in range((int(count_of_pages)//45)+1):
    soup = BeautifulSoup(response.text, 'lxml')
    data = soup.find_all('div', class_='search-item')
    for i in data:
        Flats.create(
            img_url=i.find('div', class_='image').get('src'),
            title=i.find('div', class_='title').text.strip(),
            location=i.find('div', class_='location').find('span').text.strip(),
            date_posted=i.find('span', class_='date-posted').text.strip(),
            beds=i.find('span', class_='bedrooms').text.replace(' ', '').replace('\n', '').strip()[5:],
            description=soup.find('div', class_='description').text.replace('\n', '').strip(),
            price=i.find('div', class_='price').text.strip()
        )

    browser.find_element(By.XPATH, '//*[@id="mainPageContent"]/div[3]/div[3]/main/div[2]/div[58]/div/a[10]').click()
    sleep(3)
