Parsing data from a real estate website.
Obtain information about each site and store it in a database.
Important! If you are using GoogleChrome, you need to download the driver corresponding to the version of your GoogleChrome: https://chromedriver.chromium.org/downloads
Technologies:
Python, requests, Selenium, peewee, MySQL
