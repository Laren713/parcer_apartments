from peewee import *


db = MySQLDatabase(
    'apartments', user='user', password='password',
    host='localhost'
)


class Flats(Model):
    id = PrimaryKeyField(null=False)
    img_url = CharField(default='empty')
    title = CharField(default='empty')
    location = CharField(default='empty')
    date_posted = DateField(default='empty')
    beds = CharField(default='empty')
    description = TextField(default='empty')
    price = FloatField(default='empty')

    class Meta:
        db_table = 'flats'
        database = db
